import flask
from flask.blueprints import Blueprint

from utils.db import mysql

korisnici_blueprint = Blueprint("korisnici_blueprint", __name__)

@korisnici_blueprint.route("/korisnici", methods=["GET"])
def dobavi_korisnici():
    cursor = mysql.get_db().cursor() 
    cursor.execute("SELECT * FROM korisnici") 
    korisnici = cursor.fetchall() 
    return flask.jsonify(korisnici)

@korisnici_blueprint.route("/korisnici/<int:id_korisnici>",methods=["GET"])
def dobavi_konzolu(id_korisnici):
    cursor = mysql.get_db().cursor()
    cursor.execute("SELECT * FROM korisnici WHERE id=%s", (id_korisnici,))
    korisnici = cursor.fetchone() 
    if korisnici is not None:
        return flask.jsonify(korisnici) 
    else:
        return "", 404 

@korisnici_blueprint.route("/registracija", methods=["POST"])
def dodaj_korisnika():
    db = mysql.get_db() 
    cursor = db.cursor() 
    cursor.execute("INSERT INTO korisnici(username, password, admin) VALUES(%(username)s,%(password)s,%(admin)s)", flask.request.json)
    db.commit() 
    return flask.jsonify(flask.request.json), 201

@korisnici_blueprint.route("/korisnici/<int:id_korisnici>", methods=["PUT"])
def izmeni_korisnici(id_korisnici):
    
    db = mysql.get_db()
    cursor = db.cursor()
    data = flask.request.json
    data["id"] = id_korisnici 
    cursor.execute("UPDATE korisnici SET naziv=%(naziv)s WHERE id=%(id)s", data)
    db.commit()
    return "", 200

@korisnici_blueprint.route("/korisnici/<int:id_korisnici>", methods=["DELETE"])
def ukloni_korisnici(id_korisnici):
    db = mysql.get_db()
    cursor = db.cursor()
    cursor.execute("DELETE FROM korisnici WHERE id=%s", (id_korisnici,))
    db.commit()
    return "Nije moguce obrisati!", 204 