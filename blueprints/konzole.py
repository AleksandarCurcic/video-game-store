import flask
from flask.blueprints import Blueprint

from utils.db import mysql

konzole_blueprint = Blueprint("konzole_blueprint", __name__)

@konzole_blueprint.route("/konzole", methods=["GET"])
def dobavi_konzole():
    cursor = mysql.get_db().cursor() 
    cursor.execute("SELECT * FROM konzole") 
    konzole = cursor.fetchall() 
    return flask.jsonify(konzole)

@konzole_blueprint.route("/konzole/<int:id_konzole>",methods=["GET"])
def dobavi_konzolu(id_konzole):
    cursor = mysql.get_db().cursor()
    cursor.execute("SELECT * FROM konzole WHERE id=%s", (id_konzole,))
    konzoler = cursor.fetchone() 
    if konzoler is not None:
        return flask.jsonify(konzoler) 
    else:
        return "", 404 

@konzole_blueprint.route("/konzole", methods=["POST"])
def dodaj_konzolu():
    db = mysql.get_db() 
    cursor = db.cursor() 
    cursor.execute("INSERT INTO konzole(naziv) VALUES(%(naziv)s)", flask.request.json)
    db.commit() 
    return flask.jsonify(flask.request.json), 201

@konzole_blueprint.route("/konzole/<int:id_konzole>", methods=["PUT"])
def izmeni_konzole(id_konzole):
    
    db = mysql.get_db()
    cursor = db.cursor()
    data = flask.request.json
    data["id"] = id_konzole 
    cursor.execute("UPDATE konzole SET naziv=%(naziv)s WHERE id=%(id)s", data)
    db.commit()
    return "", 200

@konzole_blueprint.route("/konzole/<int:id_konzole>", methods=["DELETE"])
def ukloni_konzoler(id_konzole):
    db = mysql.get_db()
    cursor = db.cursor()
    cursor.execute("DELETE FROM konzole WHERE id=%s", (id_konzole,))
    db.commit()
    return "Nije moguce obrisati!", 204 