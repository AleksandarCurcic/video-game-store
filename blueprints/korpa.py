import flask
from flask.blueprints import Blueprint

from utils.db import mysql

korpe_blueprint = Blueprint("korpe_blueprint", __name__)

@korpe_blueprint.route("/korpe", methods=["GET"])
def dobavi_korpe():
    cursor = mysql.get_db().cursor() 
    cursor.execute("SELECT * FROM korpe") 
    korpe = cursor.fetchall() 
    return flask.jsonify(korpe)

@korpe_blueprint.route("/korpe/<int:id_korpe>",methods=["GET"])
def dobavi_korpu(id_korpe):
    cursor = mysql.get_db().cursor()
    cursor.execute("SELECT * FROM korpe WHERE id=%s", (id_korpe,))
    korper = cursor.fetchone() 
    if korper is not None:
        return flask.jsonify(korper) 
    else:
        return "", 404 

@korpe_blueprint.route("/korpe", methods=["POST"])
def dodaj_korpu():
    db = mysql.get_db() 
    cursor = db.cursor() 
    cursor.execute("INSERT INTO korpe(kolicina) VALUES(%(naziv)s)", flask.request.json)
    db.commit() 
    return flask.jsonify(flask.request.json), 201 



@korpe_blueprint.route("/korpe/<int:id_korpe>", methods=["DELETE"])
def ukloni_artikal(id_korpe):

    
    db = mysql.get_db()
    cursor = db.cursor()
    cursor.execute("DELETE FROM korpe WHERE id=%s", (id_korpe,))
    db.commit()
    return "Nije moguce obrisati!", 204 