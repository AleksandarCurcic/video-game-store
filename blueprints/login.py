import flask
from flask import Blueprint

from utils.db import mysql

login_blueprint = Blueprint("login_blueprint", __name__)

@login_blueprint.route("/login", methods=["POST"])
def login():
    cursor = mysql.get_db().cursor()
    cursor.execute("SELECT idkorisnika, username,password, admin FROM korisnici WHERE username=%(username)s AND password=%(password)s", flask.request.json)
    korisnik = cursor.fetchone()
    print(korisnik)
    if korisnik is not None:
        flask.session["korisnik"] = korisnik["username"]
        return flask.jsonify(korisnik)
    else:
        return "", 400

@login_blueprint.route("/register", methods=["POST"])
def register():
    cursor = mysql.get_db().cursor()
    cursor.execute("INSERT INTO korisnici(username, password, admin) VALUES(%(username)s, %(password)s, %(admin)s)")
    db.commit()
    return flask.jsonify(flask.request.json), 201 


@login_blueprint.route("/logout", methods=["GET"])
def logout():
    flask.session.pop("korisnik", None) 
    return "", 200


@login_blueprint.route("/currentUser", methods=["GET"])
def current_user():
    return flask.jsonify(flask.session.get("korisnik")), 200