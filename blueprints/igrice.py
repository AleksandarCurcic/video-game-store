import flask
from flask.blueprints import Blueprint

from utils.db import mysql

igrice_blueprint = Blueprint("igrice_blueprint", __name__)

@igrice_blueprint.route("/igrice", methods=["GET"])
def dobavi_igrices():
    cursor = mysql.get_db().cursor() 
    cursor.execute("SELECT * FROM igrice INNER JOIN konzole ON igrice.idkonzole = konzole.idkonzole") 
    igrice = cursor.fetchall() 
    return flask.jsonify(igrice) 

@igrice_blueprint.route("/igrice/<int:idigrice>",methods=["GET"])
def dobavi_igricu(idigrice):
    cursor = mysql.get_db().cursor()
    cursor.execute("SELECT * FROM igrice INNER JOIN konzole ON igrice.idkonzole = konzole.idkonzole WHERE idigrice=%s", (idigrice,))
    igrica = cursor.fetchone() 
    if igrica is not None:
        return flask.jsonify(igrica) 
    else:
        return "", 404 

@igrice_blueprint.route("/igrice", methods=["POST"])
def dodaj_igricu():
    db = mysql.get_db() 
    cursor = db.cursor()
    cursor.execute("INSERT INTO igrice(naziv, cena, zanr, idkonzole, slika) VALUES(%(naziv)s, %(cena)s, %(zanr)s, %(idkonzole)s, %(slika)s)", flask.request.json)
    db.commit()
    return flask.jsonify(flask.request.json), 201 

@igrice_blueprint.route("/igrice/<int:idigrice>", methods=["PUT"])
def izmeni_igricu(idigrice):
    
    
    db = mysql.get_db()
    cursor = db.cursor()
    data = flask.request.json
    data["idigrice"] = idigrice 
    cursor.execute("UPDATE igrice SET naziv=%(naziv)s, cena=%(cena)s, zanr=%(zanr)s, idkonzole=%(idkonzole)s WHERE idigrice=%(idigrice)s", data)
    db.commit()
    return "", 200


@igrice_blueprint.route("/igrice/<int:idigrice>", methods=["DELETE"])
def ukloni_igricu(idigrice):
    db = mysql.get_db()
    cursor = db.cursor()
    cursor.execute("DELETE FROM igrice WHERE idigrice=%s", (idigrice,))
    db.commit()
    return "", 204 