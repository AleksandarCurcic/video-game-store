(function (angular) {
    var app = angular.module('app');
    app.controller('registracijaCtrl', ['loginService', '$state',"$stateParams","$rootScope", "$http", function(loginService, $state,$stateParams,$rootScope,$http) {
        var that = this;
        that.showLogin = false;
        that.failed = false;
        that.noviKorisnik = {
            'username': '',
            'password': '',
            'admin': false
        }
        that.korisnici = [];
        that.status = "neprijavljen";

        this.login = function() {
            $http.post("api/login", this.noviKorisnik).then(function (response) {
                window.localStorage.setItem("user", JSON.stringify(response.data));
                $state.go("home");
            }, function (error) {
                console.log(error);
            })
            // loginService.login(that.korisnik, function() {
            //     that.status = "prijavljen"
            //     console.log("izvrsio sam se");
            //     $state.go('home');  
            // },
            //     function () {
            //         that.failed = true;
            //     })
        }

        that.logout = function() {
            window.localStorage.removeItem("user");
        }

        this.registracija = function () {
            console.log("sadasdadads")
            $http.post("api/registracija", this.noviKorisnik).then(function (response) {
                console.log(response);
                that.noviKorisnik = response.data;
                $rootScope.korisnik = response.data;
                $state.go("home")
            }, function (reason) {
                console.log(reason);

            });
        }
        this.izmeniKorisnika = function(id) {
            $http.put("api/registracija/" + id, that.noviKorisnik).then(function(response) {
                console.log(response)
                $state.go("home")
                // Prelazak na stanje sa parametrizovanim ULR-om pri cemu
                // se vrednost parametra prosledjuje prilikom poziva
                // metode go.
            }, function(reason) {
                console.log(reason);
                // Ukoliko neprijavljeni korisnik pokusa da izmeni proizvod
                // prikazuje se stranica za prijavu.
            });
        }

        this.sacuvaj = function() {
            if($stateParams["id"]){
            this.izmeniKorisnika($stateParams["id"], that.igrice);
            }else{
                this.registracija()
            }
            
        }
        // loginService.isLoggedIn(function () {
        //     $state.go('home');
        // },
        //     function () {
        //         that.showLogin = true;
        //     });
    }]);
})(angular);