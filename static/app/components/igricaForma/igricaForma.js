(function (angular) {
    var app = angular.module("app");
    app.controller("igricaFormaCtrl", ["$http", "$state", "$stateParams", function ($http, $state, $stateParams) {
        var that = this;

        this.konzole = []; // Inicijalno kategorije nisu dobavljene.
        // Inicijalno podaci o novom lekaru su popunjeni podrazumevanim vrednostima.
        this.novaIgrica = {
            "naziv": "",
            "cena": "",
            "zanr": "",
            "idkonzole": null,
            "slika": ""
        };
        this.igrice = [];

        // Funkcija za dobavljanje svih kategorija.
        this.dobaviKonzole = function() {
            // Upucuje se get zahtev na relativni URL api/kategorije.
            $http.get("api/konzole").then(function(result){
                // Ukoliko server uspesno obradi zahtev i vrati odgovor
                // rezultat se nalazi u promenljivoj result.
                console.log(result);
                // Isparsirano telo odgovora je smesteno u promenljivu data
                // u result objektu.
                that.konzole = result.data;
                // Incijalizacija kategorije u novom lekaru
                // na prvu dobavljenu kategoriju.
                if(!$stateParams["id"]) {
                    that.novaIgrica.idkonzole = that.konzole[0].idkonzole;
                }
                 

            },
            function(reason) {
                console.log(reason);
            });
        }
        this.dobaviIgrice= function() {

            $http.get("api/igrice").then(function(result){

                console.log(result);

                that.igrice = result.data;
    
                 

            },
            function(reason) {
                console.log(reason);
            });
        }

        this.dobaviIgricu= function(id) {
            $http.get("api/igrice/" + id).then(function(result){
                that.novaIgrica = result.data;
                that.novaIgrica["idkonzole"] = that.novaIgrica["idkonzole"];
            }, function(reason) {
                console.log(reason);
            });
        }

        // Funkcija za dodavanje servisera.
        this.dodajIgricu = function () {
            // Upucuje se post zahtev na relativni URL api/igrice.
            // Uz url funkciji post se prosledjuje i objekat koji
            // treba da se prosledi u telu zahteva. Angular ce automatski
            // ovaj objekat pretvoriti u JSON format i tako ga proslediti
            // u telu zahteva.
            $http.post("api/igrice", this.novaIgrica).then(function (response) {
                console.log(response);

                $state.go("home")
            }, function (reason) {
                console.log(reason);
                // Ukoliko neprijavljeni korisnik pokusa da doda lekar
                // prikazuje se stranica za prijavu.
            });
        }
        this.izmeniIgricu = function(id) {
            $http.put("api/igrice/" + id, that.novaIgrica).then(function(response) {
                console.log(response)
                $state.go("home")
                // Prelazak na stanje sa parametrizovanim ULR-om pri cemu
                // se vrednost parametra prosledjuje prilikom poziva
                // metode go.
            }, function(reason) {
                console.log(reason);
                // Ukoliko neprijavljeni korisnik pokusa da izmeni proizvod
                // prikazuje se stranica za prijavu.
            });
        }

        this.sacuvaj = function() {
            if($stateParams["id"]){
            this.izmeniIgricu($stateParams["id"], that.igrice);
            }else{
                this.dodajIgricu()
            }
            
        }

        if($stateParams["id"]) {
            this.dobaviIgricu($stateParams["id"]);
        }
        this.dobaviKonzole();
        this.dobaviIgrice();
        if($stateParams["id"]) {
            this.dobaviIgricu($stateParams["id"]);
        }
    }]);
})(angular);