(function (angular) {
    var app = angular.module('app');
    app.controller('loginCtrl', ['loginService', '$state', '$http', function(loginService, $state, $http) {
        var that = this;
        that.showLogin = false;
        that.failed = false;
        that.noviKorisnik = {
            'username': '',
            'password': '',
            'admin': false
        }
        this.korisnik = window.localStorage.getItem("user") ? true : false;
        that.status = "neprijavljen";
        console.log(that.status);

        this.login = function() {
            console.log("login")
            
            $http.post("api/login", this.noviKorisnik).then(function (response) {
                window.localStorage.setItem("user", JSON.stringify(response.data));
                $state.go("home");
            }, function (error) {
                console.log(error);
            })
            // loginService.login(that.korisnik, function() {
            //     that.status = "prijavljen"
            //     console.log("izvrsio sam se");
            //     $state.go('home');  
            // },
            //     function () {
            //         that.failed = true;
            //     })
        }

        that.logout = function() {
            console.log("logut")
            that.status = "neprijavljen";
            window.localStorage.removeItem("user");
            $state.go("home");
        }

        this.registracija = function () {
            $http.post("api/registracija", this.noviKorisnik).then(function (response) {
                console.log(response);
                that.noviKorisnik = response.data;
                $rootScope.korisnik = response.data;
                $state.go("home")
            }, function (reason) {
                console.log(reason);

            });
        }
        this.izmeniIgricu = function(id) {
            $http.put("api/igrice/" + id, that.novaIgrica).then(function(response) {
                console.log(response)
                $state.go("home")
                // Prelazak na stanje sa parametrizovanim ULR-om pri cemu
                // se vrednost parametra prosledjuje prilikom poziva
                // metode go.
            }, function(reason) {
                console.log(reason);
                // Ukoliko neprijavljeni korisnik pokusa da izmeni proizvod
                // prikazuje se stranica za prijavu.
            });
        }
        // loginService.isLoggedIn(function () {
        //     $state.go('home');
        // },
        //     function () {
        //         that.showLogin = true;
        //     });
    }]);
})(angular);