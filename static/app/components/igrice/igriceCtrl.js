(function (angular) {
    // Dobavljanje postojeceg modula app.
    var app = angular.module("app");
    app.controller("IgriceCtrl", ["$http", "$scope", "$state", function ($http, $scope, $state) {
        var that = this; 

        this.igrice = []; // Inicijalno proizvodi nisu dobavljeni.
        this.konzole = [];
        this.search = "";
        this.korisnik = window.localStorage.getItem("user") ? true : false;
        this.query = '';
        this.queryzanr = '';
        
        // this.search = function () {
        //     if (that.igrice.length <= 0 || that.query === "") {
        //         alert("NEKI ALERT")
        //     }
        //     var arr = igrice.filter(igrica => igrica.naziv.toLowerCase() === that.query.toLowerCase());
        //     console.log(arr);
        // }
        // Funkcija za dobavljanje proizvoda.
        this.dobaviIgrice = function () {
            // Upucuje se get zahtev na relativni URL api/proizvodi.
            $http.get("api/igrice").then(function (result) {
                that.igrice = result.data;
            },
                function (reason) {
                    console.log(reason);
                });
        }
        this.dobaviKonzole = function () {
            // Upucuje se get zahtev na relativni URL api/proizvodi.
            $http.get("api/konzole").then(function (result) {
                console.log(result);
                that.konzole = result.data;
            },
                function (reason) {
                    console.log(reason);
                });
        }

        this.ukloniIgricu = function (id) {
            $http.delete("api/igrice/" + id).then(function (response) {
                console.log(response);
                that.dobaviIgrice();
            },
                function (reason) {
                    console.log(reason);

                });
        }
        this.dobaviIgrice();
        this.dobaviKonzole();
    }]);
})(angular);