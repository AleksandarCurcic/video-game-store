(function(angular){
    var app = angular.module("app");
    app.controller("prikazIgreCtrl", ["$http", "$stateParams", "$state", function($http, $stateParams, $state) {
        var that = this;
        this.igra = {};
        this.korisnik = window.localStorage.getItem("user") ? true : false;

        this.dobaviIgru = function(id) {
            $http.get("api/igrice/" + id).then(function(result){
                console.log(result);
                that.igra = result.data;
            }, function(reason) {
                console.log(reason);
            });
        }
        this.izmeniIgricu = function(id) {
            $http.put("api/igrice/" + id, that.novaIgrica).then(function(response) {
                console.log(response)
                $state.go("home")
            }, function(reason) {
                console.log(reason);
                
            });
        }
        

        this.dobaviIgru($stateParams["id"]);
    }]);
})(angular)