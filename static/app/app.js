(function(angular){
    // Kreiranje modula pod nazivom app koji kao zavisnost ima ui router modul.
    // U slucaju da je modul vec kreiran, umesto kreiranja novog, bice
    // dobavljen postojeci modul.
    var app = angular.module("app", ["ui.router","loginService"]);

    app.config(["$stateProvider", "$urlRouterProvider", function($stateProvider, $urlRouterProvider) {
        // Stanje se registruje preko $stateProvider servisa, pozivom
        // metode state kojoj se prosledjuje state objekat.
        $stateProvider.state({
            name: "home", // Naziv stanja, mora biti zadat.
            url: "/", // URL kojim se prelazi na zadato stanje.
            templateUrl: "app/components/igrice/igrice.tpl.html", // URL do sablona koji se koristi za zadato stanje.
            controller: "IgriceCtrl", // Kontroler koji se vezuje za sablon u zadatom stanju.
            controllerAs: "pctrl" // Naziv vezanog kontrolera.
        }).state({
            name: "dodajIgricu",
            url: "/igricaForma",
            templateUrl: "app/components/igricaForma/igricaForma.tpl.html",
            controller: "igricaFormaCtrl",
            controllerAs: "pctrl"
        }).state({
            name: "login",
            url: "/login",
            templateUrl: "app/components/login/login.tpl.html",
            controller: "loginCtrl",
            controllerAs: "lctrl"
        }).state({
            name: "prikaz",
            url: "/igrica/{id}", 
            templateUrl: "app/components/prikazIgrice/prikazIgrice.tpl.html",
            controller: "prikazIgreCtrl",
            controllerAs: "ctrlp"
        }).state({
            name: "izmenaIgrice",
            url: "/igricaForma/{id}",
            templateUrl: "app/components/igricaForma/igricaForma.tpl.html",
            controller: "igricaFormaCtrl", // Isti kontroler se moze koristiti vise puta.
            controllerAs: "pctrl"
        }).state({
            name: "registracija",
            url: "/registracija",
            templateUrl: "app/components/registracija/registracija.tpl.html",
            controller: "registracijaCtrl", // Isti kontroler se moze koristiti vise puta.
            controllerAs: "rctrl"
        }).state({
            name: "izmenaKorisnika",
            url: "/login/izmena",
            templateUrl: "app/components/izmenaKorisnika/izmenaKorisnika.tpl.html",
            controller: "izmenaCtrl", // Isti kontroler se moze koristiti vise puta.
            controllerAs: "ictrl"
        });

        // Ukoliko zadati url ne pokazuje ni na jedno stanje
        // vrsi se preusmeravajne na url zadat pozivom metode
        // otherwise nad $urlRouterProvider servisom.
        $urlRouterProvider.otherwise("/")
    }])
})(angular);