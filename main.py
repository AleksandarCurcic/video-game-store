import flask
import datetime
from flask import Flask
from utils.db import mysql
from blueprints.igrice import igrice_blueprint
from blueprints.konzole import konzole_blueprint
from blueprints.login import login_blueprint
from blueprints.korisnici import korisnici_blueprint


app = Flask(__name__, static_url_path="")

app.config["MYSQL_DATABASE_USER"] = "root" 
app.config["MYSQL_DATABASE_PASSWORD"] = "armagedon1"
app.config["MYSQL_DATABASE_DB"] = "Prodavnica2"
app.config["SECRET_KEY"] = "ta WJoir29$"
mysql.init_app(app) 
app.register_blueprint(igrice_blueprint, url_prefix="/api")
app.register_blueprint(konzole_blueprint, url_prefix="/api")
app.register_blueprint(login_blueprint, url_prefix="/api")
app.register_blueprint(korisnici_blueprint, url_prefix="/api")

@app.route("/")
@app.route("/index")
def index_page():
    return app.send_static_file("index.html")

if __name__ == "__main__":
    app.run("0.0.0.0", 5000, threaded=True)
